from dotenv import load_dotenv
from proxmoxer.core import ResourceException

from utils import get_netbox_description, update_interface, update_ip, update_vm
load_dotenv()
from proxmoxer import ProxmoxAPI
import pynetbox
from os import environ
from loguru import logger
import re
proxmox = ProxmoxAPI(environ.get('PROXMOX_HOST'), user=environ.get('PROXMOX_USER'), password=environ.get('PROXMOX_PASSWORD'), verify_ssl=False)
netbox = pynetbox.api(environ.get('NETBOX_URL'), token=environ.get('NETBOX_TOKEN'))

# Cluster id
cluster_id = 1
tenant_id = 2

netbox_vm = netbox.virtualization.virtual_machines.get(name='okd-master3')

# Create an object {'prometheus': 'prod'}
vm_pools = {}
for pool in proxmox.pools.get():
  for vm in proxmox.pools(pool['poolid']).get()['members']:
    if vm['type'] == 'qemu':
      vm_pools[vm['name']] = pool['poolid']


for node in proxmox.nodes.get():
  if node['status'] != 'online':
    continue

  for vm in proxmox.nodes(node['node']).qemu.get():
    logger.debug(f'Sync {vm["name"]}')
    vm_config = proxmox.nodes(node['node']).qemu(vm['vmid']).config.get()

    # Define the vm_interfaces if the agent is running
    vm_interfaces = None
    try:
      vm_interfaces = proxmox.nodes(node['node']).qemu(vm['vmid']).agent.get('network-get-interfaces')
    except ResourceException:
      pass

    disk_size = 0
    device_id = 0
    # Retrieve all the disks
    while f'scsi{device_id}' in vm_config:
      # Parses the string. Example sas_vm:vm-152-disk-0,size=120G
      size, unit = re.search(r'size=(\d+)([GTM])', vm_config[f'scsi{device_id}']).groups()

      size = int(size)
      if unit == 'M':
        size = 0 # Netbox only accept round GB number, so MB doesn't count
      elif unit == 'T':
        size *= 1000 # If TB, multiply the number by 1000
      
      disk_size += size
      device_id += 1 


    # Create the vm body
    vm_body = {
      'name': vm['name'], 
      'vcpus': vm['cpus'], 
      'memory': vm['maxmem'] / (1024 ** 2), # Convert from bytes to MiB
      'disk': disk_size,
      'cluster': cluster_id, # Proxmox SIA
      'tenant': tenant_id, # SIA
      'status': 'active' if vm['status'] == 'running' else 'offline',
      'comments': get_netbox_description()
    }

    # Add the role if exists
    if vm['name'] in vm_pools:
      vm_body['role'] = {'slug': vm_pools[vm['name']]}


    primary_ip_id = 0
    netbox_vm = update_vm(netbox, vm['name'], vm_body)

    # Retrive all interfaces
    device_id = 0
    while f'net{device_id}' in vm_config:
      # Convert the string in array of objects. Example virtio=f2:3a:f9:a9:74:fb,bridge=vmbr0,tag=40

      interface_attrs = {}
      for attribute in vm_config[f'net{device_id}'].split(','):
        label, value = attribute.split('=')
        interface_attrs[label] = value

        # The mac is either in virtio (linux) or e1000 (windows)
        if label in ('virtio', 'e1000'):
          interface_attrs['mac'] = value

      interface_body = {
        'name': f'net{device_id}',
        'virtual_machine': netbox_vm.id,
        'mac_address': interface_attrs['mac'],
        'mode': 'access',
        'description': get_netbox_description(),
        'enabled': 'link_down' not in interface_attrs,
        'custom_fields': {}
      }

      # Add the vlan tag if exists
      if 'tag' in interface_attrs:
        vlan = int(interface_attrs['tag'])
        netbox_vlan = netbox.ipam.vlans.get(vid=vlan)

        if netbox_vlan:
          interface_body['untagged_vlan'] = netbox_vlan.id
        else:
          logger.warning(f'VLAN {vlan} does not exists in Netbox')

      ip_bodies = []

      # Try to find VM interfaces
      if vm_interfaces:
        for vm_interface in vm_interfaces['result']:
          # Skip if the interface is not eth0 or ens18 like
          if not vm_interface['name'].startswith(('eth', 'ens')):
            continue
          
          # Skip if the interface mac address is not equal to the vm mac address
          if vm_interface['hardware-address'] != interface_attrs['mac'].lower():
            continue

          # Set the VM interface name
          interface_body['custom_fields']['vm_interface'] = vm_interface['name']


          # For each ip
          for ip in vm_interface['ip-addresses']:
            # Skip if ipv6
            if ip['ip-address-type'] != 'ipv4':
              continue

            ip_body = {
              'address': f"{ip['ip-address']}/{ip['prefix']}",
              'description': get_netbox_description(),
              'status': 'active',
              'dns_name': vm['name'],
              'tenant': tenant_id,
              'assigned_object_type': 'virtualization.vminterface',
            }

            # Create ip body and append to be used to create the netbox ip
            ip_bodies.append(ip_body)

            


      # Create/update the interface
      netbox_interface = update_interface(netbox, f'net{device_id}', netbox_vm.id, interface_body)

      for ip_body in ip_bodies:
        ip_body['assigned_object_id'] = netbox_interface.id
        netbox_ip = update_ip(netbox, netbox_interface.id, ip_body)

        # Update the primary ip id if it is the first one and is not a VIP
        if primary_ip_id == 0 and not (netbox_ip.role and netbox_ip.role.value == 'vip'):
          primary_ip_id = netbox_ip.id

      device_id += 1

    # Update the primary ip if different and exists or does not exists
    if primary_ip_id != 0 and (not netbox_vm.primary_ip4 or primary_ip_id != netbox_vm.primary_ip4.id):
      vm_body['primary_ip4'] = primary_ip_id
      update_vm(netbox, vm['name'], vm_body)


# Delete old vms

proxmox_vms = []
for node in proxmox.nodes.get():
  if node['status'] != 'online':
    continue

  for vm in proxmox.nodes(node['node']).qemu.get():
    proxmox_vms.append(vm['name'])


netbox_vms = netbox.virtualization.virtual_machines.filter(cluster_id=cluster_id)
for netbox_vm in netbox_vms:
  if netbox_vm.name not in proxmox_vms:
    logger.warning(f'Delete {netbox_vm.name}')
    netbox_vm.delete()
