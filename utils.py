from datetime import datetime
from loguru import logger

def get_netbox_description():
  return datetime.now().strftime("Synced on %d/%m/%Y at %H:%M:%S")

def update_vm(netbox, vm_name, vm_body):
  netbox_vm = netbox.virtualization.virtual_machines.get(name=vm_name)

  # If the vm is already created, update it
  if netbox_vm:
    vm_body['id'] = netbox_vm.id

    netbox.virtualization.virtual_machines.update([vm_body])

  else:
    logger.info(f'Create netbox vm {vm_name}')
    [netbox_vm] = netbox.virtualization.virtual_machines.create([vm_body])

  return netbox_vm

def update_interface(netbox, interface_name, vm_id, interface_body):
  netbox_interface = netbox.virtualization.interfaces.get(name=interface_name, virtual_machine_id=vm_id)

  if netbox_interface:
    interface_body['id'] = netbox_interface.id

    netbox.virtualization.interfaces.update([interface_body])

  else:
    logger.info(f'Create netbox interface {interface_name}')
    [netbox_interface] = netbox.virtualization.interfaces.create([interface_body])

  return netbox_interface


def update_ip(netbox, interface_id, ip_body):
  netbox_ip = netbox.ipam.ip_addresses.get(address=ip_body['address'])
  # If the ip exists
  if netbox_ip:
    
    # Skip if the IP is a VIP
    if netbox_ip.role and netbox_ip.role.value == 'vip':
      logger.info(f'Skip sync VIP address {ip_body["address"]}')
      return netbox_ip

    # If the ip is attached to somthing that is not the vm
    if netbox_ip.assigned_object_type == 'virtualization.vminterface' and netbox_ip.assigned_object_id and netbox_ip.assigned_object_id != interface_id:
      raise Exception(f'The ip {netbox_ip.address} is already bound to another vm, you have to change update it manually to avoid mistakes. Conflict interface id : {netbox_ip.assigned_object_id}')

    ip_body['id'] = netbox_ip.id


    netbox.ipam.ip_addresses.update([ip_body])
  
  else:
    logger.info(f'Create netbox ip {ip_body["address"]}')
    [netbox_ip] = netbox.ipam.ip_addresses.create([ip_body])

  return netbox_ip